import time


def extendedEuclidean(a, b):
    x = 0
    y = 0
    xi_2 = 1
    xi_1 = 0
    yi_2 = 0
    yi_1 = 1
    q = 0
    r = 0
    d = 0
    i = 1
    start = time.time()
    while True:
        r = a % b
        if r == 0:
            break

        q = int(a / b)
        a = b
        b = r
        d = b

        x = int(xi_2 - (q * xi_1))
        xi_2 = int(xi_1)
        xi_1 = int(x)

        y = int(yi_2 - (q * yi_1))
        yi_2 = int(yi_1)
        yi_1 = int(y)

        print("%d.\tr = %d, x = %d, y = %d" % (i, r, x, y))
        i += 1

    end = time.time()
    print("d = %d" % (d))
    print("x = %d" % (x))
    print("y = %d" % (y))
    print("время:", 1000 * (end - start), "мс")


def extendedBinaryEuclidean(a, b):
    u, v = a, b
    x = 0
    y = 0
    xi_2 = 1
    xi_1 = 0
    yi_2 = 0
    yi_1 = 1
    i = 1
    g = 1
    start = time.time()
    while u != 0:
        while (u & 1) == 0:
            u >>= 1
            if ((xi_2 & 1) == 0 and (xi_1 & 1) == 0):
                xi_2 >>= 1
                xi_1 >>= 1
            else:
                xi_2 = (xi_2 + b) >> 1
                xi_1 = (xi_1 - a) >> 1
        while (v & 1) == 0:
            v >>= 1
            if ((yi_2 & 1) == 0 and (yi_1 & 1) == 0):
                yi_2 >>= 1
                yi_1 >>= 1
            else:
                yi_2 = (yi_2 + b) >> 1
                yi_1 = (yi_1 - a) >> 1
        if (u >= v):
            u -= v
            xi_2 -= yi_2
            xi_1 -= yi_1
        else:
            v -= u
            yi_2 -= xi_2
            yi_1 -= xi_1
        print("%d.\tu = %d, v = %d, x = %d, y = %d" % (i, a, b, yi_2, yi_1))
        i += 1

    x = yi_2
    y = yi_1
    d = g * v

    end = time.time()
    print("d = %d" % (d))
    print("x = %d" % (x))
    print("y = %d" % (y))
    print("время:", 1000 * (end - start), "мс")


def extendedEuclideanUO(a, b):
    x = 0
    y = 0
    xi_2 = 1
    xi_1 = 0
    yi_2 = 0
    yi_1 = 1
    q = 0
    r = b
    d = 0
    i = 1
    start = time.time()
    while True:

        if (a % b) > r / 2:
            r -= a % b

            if r == 0:
                break
            q = int((a - r) / b)
            a = b
            b = r
            d = b

            x = int(xi_2 - (q * xi_1))
            x = xi_1 - x
            xi_2 = int(xi_1)
            xi_1 = int(x)

            y = int(yi_2 - (q * yi_1))
            y = yi_1 - y
            yi_2 = int(yi_1)
            yi_1 = int(y)

            print("%d.\tr = %d, x = %d, y = %d" % (i, r, x, y))
            i += 1

        else:
            r = a % b

            if r == 0:
                break

            q = int((a - r) / b)
            a = b
            b = r
            d = b

            x = int(xi_2 - (q * xi_1))
            xi_2 = int(xi_1)
            xi_1 = int(x)

            y = int(yi_2 - (q * yi_1))
            yi_2 = int(yi_1)
            yi_1 = int(y)

            print("%d.\tr = %d, x = %d, y = %d" % (i, r, x, y))
            i += 1

    end = time.time()
    print("d = %d" % (d))
    print("x = %d" % (x))
    print("y = %d" % (y))
    print("время:", 1000 * (end - start), "мс")


if __name__ == '__main__':
    a1 = 17481293748488705557
    b1 = 17481291639152538259
    a2 = 366955414713773359337098382781682083499
    b2 = 299668074543580338569260490955443496859
    a3 = 41105491687986911935856926665691767842102156594917911152113140841546153923976651
    b3 = 24317287522420913836543113811721506137522578408656635485699151759739758575817973

    # extendedEuclidean(a1, b1)
    # extendedEuclidean(a2, b2)
    # extendedEuclidean(a3, b3)

    # extendedBinaryEuclidean(a1, b1)
    # extendedBinaryEuclidean(a2, b2)
    # extendedBinaryEuclidean(a3, b3)

    # extendedEuclideanUO(a1, b1)
    # extendedEuclideanUO(a2, b2)
    extendedEuclideanUO(a3, b3)

    # x = a3 * 872558544984882773412262121167511609344 + b3 * -1474956776535547972882167965719166400597
    # print(x)

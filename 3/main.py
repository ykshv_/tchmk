from random import randint
import time
import math

n1 = 3692656043959591931
n2 = 1929226352603154375160170839570465548571
n3 = 19233835149027912156382238855284218685967993959587426296479131919012071872625521

rows, cols = (5, 4)
array = [[0] * cols for _ in range(rows)]
prime_num = 2
file = open("primes.txt", "r")


def x_func(x, n):
    return (((x ** 2) + 1) % n)


def next_prime():
    line = file.readline()
    p = int(line)
    return p


def ro_pollard(n):
    file = open('ro_pollard.txt', 'a')
    x = randint(0, n - 1)
    file.write("Начальное значение: " + str(x) + "\n")
    file.write("Отображение: " + "x^2 + 1 (mod n)" + "\n")
    a = x
    b = x
    i = 0
    start = time.time()
    while (True):
        if (time.time() - start > 7200):
            for i in range(5):
                file.write(str(array[i][0]) + ". a = " + str(array[i][1]) + " b = " +
                           str(array[i][2]) + " d = " + str(array[i][3]) + "\n")
            file.write("\n\n")
            return -1
        i += 1
        a = x_func(a, n)
        b = x_func(x_func(b, n), n)
        d = math.gcd(a - b, n)
        if d == n:
            print("Делитель не найден")
            file.close()
            break
        if (i < 6):
            file.write(str(i) + ". a = " + str(a) + " b = " +
                       str(b) + " d = " + str(d) + "\n")
        else:
            for j in range(4):
                array[j] = array[j + 1][:]
            array[4][0] = i
            array[4][1] = a
            array[4][2] = b
            array[4][3] = d
        if d > 1 and d < n:
            print(time.time() - start)
            for i in range(5):
                file.write(str(array[i][0]) + ". a = " + str(array[i][1]) + " b = " +
                           str(array[i][2]) + " d = " + str(array[i][3]) + "\n")
            file.write("\n\n")
            file.close()
            break


def ro_1_pollard(n):
    a = randint(2, n - 2)
    d = math.gcd(a, n)
    if d >= 2:
        print(d)
        return
    file = open('ro_1_pollard.txt', 'a')
    i = 1
    prime_number = next_prime()
    file.write(str(i) + ". База " +
               str(prime_number) + " l_i не вычеслено(первый шаг)\n")
    start = time.time()
    while (i < 10000000000):
        if (time.time() - start > 7200):
            for i in range(3):
                file.write(str(array[i][0]) + ". база = " + str(array[i][1]) + " l_i = " +
                           str(array[i][2]) + "\n")
            file.write("\n\n")
            return -1
        l = int(math.log(n)/math.log(prime_number))
        tmp = int(prime_number ** l)
        a = pow(a, tmp, n)
        d = math.gcd(a - 1, n)
        if (i < 6):
            file.write(str(i) + ". база = " + str(prime_number) + " l_i = " +
                       str(l) + "\n")
        else:
            for j in range(3):
                array[j] = array[j + 1][:]
            array[3][0] = i
            array[3][1] = prime_number
            array[3][2] = l
        if (d > 1 and d < n):
            for j in range(3):
                file.write(str(array[j][0]) + ". база = " + str(array[j][1]) + " l_i = " +
                           str(array[j][2]) + "\nd = " + str(d) + "\n")
            file.write("\n\n")
            print(time.time() - start)
            return
        i += 1
        prime_number = next_prime()


if __name__ == '__main__':
    # ro_pollard(n1)
    # ro_pollard(n2)
    # ro_pollard(n3)
    # ro_1_pollard(n1)
    # ro_1_pollard(n2)
    # ro_1_pollard(n3)
    file.close()

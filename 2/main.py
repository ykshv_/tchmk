from random import randint
from sympy import jacobi_symbol

osnovaniya = []


def testFerma(n):
    a = randint(2, n - 2)
    osnovaniya.append(a)
    r = pow(a, n - 1, n)
    if r == 1:
        return True
    return False


def testSolovey(n):
    a = randint(2, n - 2)
    osnovaniya.append(a)
    r = pow(a, (n - 1) // 2, n)
    if r != 1 and r != n - 1:
        print("Нарушено условие r != 1 и r != n-1")
        return False
    s = jacobi_symbol(a, n)
    if r != s % n:
        print("Нарушено условие, символ Якоби не равен r")
        return False
    return True


def testRabin(n):
    r = n - 1
    s = 0
    a = randint(2, n - 2)
    osnovaniya.append(a)
    while r % 2 == 0:
        r //= 2
        s += 1
    y = pow(a, r, n)
    if y != 1 and y != n - 1:
        j = 1
        while (j <= s - 1 and y != n - 1):
            y = pow(y, 2, n)
            if (y == 1):
                print("Нарушено условие: y == 1")
                return False
            j += 1
        if y != n - 1:
            print("Нарушено условие y != n-1")
            return False
    return True


def Ferma(n):
    prost = True
    for i in range(0, 5):
        if testFerma(n) == False:
            print("%d - составное число" % (n))
            prost = False
            print(osnovaniya[i])
            break
    if prost:
        print("%d - вероятно простое число" % (n))
        print(osnovaniya)


def Solovey(n):
    prost = True
    for i in range(0, 5):
        if testSolovey(n) == False:
            print("%d - составное число" % (n))
            prost = False
            print(osnovaniya[i])
            break
    if prost:
        print("%d - вероятно простое число" % (n))
        print(osnovaniya)


def Rabin(n):
    prost = True
    for i in range(0, 5):
        if testRabin(n) == False:
            print("%d - составное число" % (n))
            prost = False
            print(osnovaniya[i])
            break
    if prost:
        print("%d - вероятно простое число" % (n))
        print(osnovaniya)


if __name__ == '__main__':
    a1 = 48137464161188165521
    a2 = 9170542013023227011797592809245678705139
    a3 = 1270133035350823536863413658910477682663
    a4 = 46609468952382324147259847273376977954351444806709220721459221109910068375277983

    kr1 = 32809426840359564991177172754241
    kr2 = 2810864562635368426005268142616001
    kr3 = 349407515342287435050603204719587201

    # Ferma(a1)
    # Ferma(a2)
    # Ferma(a3)
    # Ferma(a4)
    # Ferma(kr1)
    # Ferma(kr2)
    # Ferma(kr3)

    # Solovey(a1)
    # Solovey(a2)
    # Solovey(a3)
    # Solovey(a4)
    # Solovey(kr1)
    # Solovey(kr2)
    # Solovey(kr3)

    # Rabin(a1)
    # Rabin(a2)
    # Rabin(a3)
    # Rabin(a4)
    # Rabin(kr1)
    # Rabin(kr2)
    # Rabin(kr3)

    # print(jacobi_symbol(22473335841418673276, 48137464161188165521))

    # print(pow(22473335841418673276, (48137464161188165521 - 1) // 2, 48137464161188165521))

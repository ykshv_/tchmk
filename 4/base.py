import time
import random
import sympy
import math


def solve_linear_congruence(a, b, m):
    g = math.gcd(a, m)
    if b % g:
        raise ValueError('No solutions')
    a, b, m = a // g, b // g, m // g
    return pow(a, -1, m) * b % m


def print_solutions(a, b, m):
    try:
        x = solve_linear_congruence(a, b, m)
    except ValueError:
        return -1
    else:
        return x


def IsPowerSmooth(B, num, size):
    base = [0 for i in range(size)]
    for i in range(size):
        while True:
            if num % B[i] == 0:
                base[i] += 1
                num //= B[i]
            else:
                break
        if num == 1:
            return True, base
    return False, base


def Base(a, b, p, q):
    B = []
    U = []
    y = math.exp((math.log1p(p)*math.log1p(math.log1p(p)))**(0.5))
    with open('primes.txt') as f:
        num = int(f.readline())
        while num < y:
            B.append(num)
            num = int(f.readline())

    degrees = []
    a_ui = []
    while len(degrees) != len(B):
        if time.time() - start_time > 7200:
            file = open("base.txt", "a")
            file.write("2 часа прошло\n")
            file.write("#B = " + str(len(B)) + "\n")
            file.write("B = " + str(B[-1]) + "\n")
            file.write("#a^u = " + str(len(degrees)) + "\n")
            file.write("a^u = " + str(a_ui[-1]) + "\n")
            file.write("u = " + str(degrees[-1]) + "\n")
            file.write("Векторы показателя a^u: ")
            file.write(str(U[-1]))
            file.close()
            return True
        degree = random.randint(0, p - 1)
        tmp = IsPowerSmooth(B, pow(a, degree, p), len(B))
        if tmp[0]:
            if degree in degrees or tmp[1] in U:
                continue
            a_ui.append(pow(a, degree, p))
            degrees.append(degree)
            U.append(tmp[1])

    v = 1
    while True:
        tmp = IsPowerSmooth(B, (b ** v) % p, len(B))
        if tmp[0]:
            V = tmp[1]
            break
        v += 1

    if time.time() - start_time > 7200:
        file = open("base.txt", "a")
        file.write("2 часа прошло\n")
        file.write("#B = " + str(len(B)) + "\n")
        file.write("B = " + str(B[-1]) + "\n")
        file.write("#a^u = " + str(len(degrees)) + "\n")
        file.write("a^u = " + str(a_ui[-1]) + "\n")
        file.write("u = " + str(degrees[-1]) + "\n")
        file.write("Векторы показателя a^u: ")
        file.write(str(U[-1]) + "\n")
        file.write("v = " + str(v) + "\n")
        file.write("Векторы показателя b^v: ")
        file.write(str(V[-1]) + "\n")
        file.close()
        return True

    aa = sympy.Matrix(U)
    bb = sympy.Matrix(degrees)
    det = int(aa.det())
    ans = 0
    if math.gcd(det, p) == 1:
        ans = pow(det, -1, p) * aa.adjugate() @ bb % p
    else:
        return False

    x = 0
    for i in range(len(B)):
        x += V[i] * ans[i]
    flag = print_solutions(v, x, q)

    if flag == -1:
        return False

    else:
        if pow(a, int(flag), p) != b:
            return False
        print(f'x = {flag}')
        print("#B:", len(B))
        print(f'Последний элемент B = {B[-1]}')
        print("a^u: ", *a_ui[0:5])
        print("u:   ", *degrees[0:5])
        print("Векторы показателей a^u: ")
        for i in range(5):
            print(U[i])
        print("v =", v)
        print('Вектор показателей для b^v:', *V)
        return True


a = 3
b = 5
p = 799454910203247398939
q = 399727455101623699469
# a = 7
# b = 151
# p = 547
# q = 546
# a = 2
# b = 7
# p = 137
# q = 68

flag = False
start_time = time.time()
while (flag == False):
    flag = Base(a, b, p, q)

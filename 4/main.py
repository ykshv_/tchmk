import math
import time

a = 2
b = 7
p = 137
q = 68

rows = 5
columns = 8
array = [[0] * columns for _ in range(rows)]


def extendedEuclideanUO(a1, b1):
    if math.gcd(a1, b1) != 1:
        return 1
    if b1 > a1:
        b1 = b1 % a1
    x = 0
    y = 0
    xi_2 = 1
    xi_1 = 0
    yi_2 = 0
    yi_1 = 1
    q1 = 0
    r = b1
    while True:
        if (a1 % b1) > r / 2:
            r -= a1 % b1
            if r == 0:
                break
            q1 = int((a1 - r) / b1)
            a1 = b1
            b1 = r
            x = int(xi_2 - (q1 * xi_1))
            x = xi_1 - x
            xi_2 = int(xi_1)
            xi_1 = int(x)

            y = int(yi_2 - (q1 * yi_1))
            y = yi_1 - y
            yi_2 = int(yi_1)
            yi_1 = int(y)
        else:
            r = a1 % b1

            if r == 0:
                break

            q1 = int((a1 - r) / b1)
            a1 = b1
            b1 = r

            x = int(xi_2 - (q1 * xi_1))
            xi_2 = int(xi_1)
            xi_1 = int(x)

            y = int(yi_2 - (q1 * yi_1))
            yi_2 = int(yi_1)
            yi_1 = int(y)

    return y


def solve(coef, sv_num, m):
    if coef == 0 or sv_num == 0 or m == 0:
        print("error")
        exit()
    if coef < 0:
        coef = -coef
        sv_num = -sv_num
    x = extendedEuclideanUO(m, coef)
    return (x * sv_num) % m


def display(c, log_x, log_number):
    if c >= p // 2:
        return (a * c) % p, log_x, log_number + 1
    else:
        return (b * c) % p, log_x + 1, log_number


def ro_pollard():
    file = open("ro_pollard.txt", "a")
    u, v = 2, 2
    c = ((a ** u) * (b ** v)) % p
    d = c
    logc_x = 2
    logc_number = 2
    logd_x = 2
    logd_number = 2
    i = 0
    file.write(str(i) + ". c = " + str(c) + " d = " + str(d) + " log_a(c) = " + str(logc_x) +
               "x + " + str(logc_number) + " log_a(d) = " + str(logd_x) + "x + " + str(logd_number) + "\n")
    start = time.time()
    end = 0
    while 1:
        if time.time() - start > 7200:
            for j in range(5):
                file.write(str(array[j][0]) + ". c = " + str(array[j][1]) + " d = " + str(array[j][2]) + " log_a(c) = " + str(array[j][3]) +
                           "x + " + str(array[j][4]) + " log_a(d) = " + str(array[j][5]) + "x + " + str(array[j][6]) + " time = " + str(array[j][7]) + "\n")
            file.close()
            return
        i += 1
        c, logc_x, logc_number = display(c, logc_x, logc_number)
        d, logd_x, logd_number = display(*display(d, logd_x, logd_number))
        end = time.time() - start
        if (i < 6):
            file.write(str(i) + ". c = " + str(c) + " d = " + str(d) + " log_a(c) = " + str(logc_x) +
                       "x + " + str(logc_number) + " log_a(d) = " + str(logd_x) + "x + " + str(logd_number) + " time = " + str(end) + "\n")
        else:
            for j in range(4):
                array[j] = array[j + 1][:]
            array[4][0] = i
            array[4][1] = c
            array[4][2] = d
            array[4][3] = logc_x
            array[4][4] = logc_number
            array[4][5] = logd_x
            array[4][6] = logd_number
            array[4][7] = end

        if c % p is d % p:
            for j in range(5):
                file.write(str(array[j][0]) + ". c = " + str(array[j][1]) + " d = " + str(array[j][2]) + " log_a(c) = " + str(array[j][3]) +
                           "x + " + str(array[j][4]) + " log_a(d) = " + str(array[j][5]) + "x + " + str(array[j][6]) + " time = " + str(array[j][7]) + "\n")
            print(solve(logc_x - logd_x, logd_number - logc_number, q))
            file.close()
            return


if __name__ == '__main__':
    ro_pollard()

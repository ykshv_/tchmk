from collections import OrderedDict
import math
import time


rows = 5
columns = 2

array = [[0] * columns for _ in range(rows)]


def gcdExtended(a, b):
    if a == 0:
        return b, 0, 1
    gcd, x1, y1 = gcdExtended(b % a, a)
    x = y1 - (b//a) * x1
    y = x1
    return gcd, x, y


def reverseMod(a, m):
    gcd, x, y = gcdExtended(a, m)
    if gcd == 1:
        return (x % m + m) % m
    else:
        return -1


# def Gelfond1(a, b, p, q):
#     file = open("gelfond.txt", "a")
#     s = int(math.sqrt(q) + 0.5) + 1
#     s_1 = reverseMod(pow(a, s, p), p)
#     vals = []
#     start = time.time()
#     for i in range(0, s):
#         if time.time() - start > 7200:
#             for i in range(5):
#                 file.write("{ " + str(i) + ", " + str(vals[i]) + " }\n")
#             file.write("\n")
#             for i in vals[-5:]:
#                 file.write("{ " + str(i) + ", " + str(vals[i]) + " }\n")
#             file.write("\n")
#             file.close()
#             return -1
#         vals.append((b * pow(s_1, i, p)) % p)
#
#     base_time = time.time()
#     for i in range(5):
#         file.write("{ " + str(i) + ", " + str(vals[i]) + " }\n")
#     file.write("\n")
#     for index, value in enumerate(vals[-5:]):
#         file.write("{ " + str(len(vals) - 5 + index) +
#                    ", " + str(value) + " }\n")
#     file.write("\n")
#
#     for i in range(0, s):
#         if time.time() - start > 7200:
#             file.write(
#                 "t, до которого построена последовательность a^t (mod p): " + str(i) + "\n")
#             file.write("время затраченное на построение второй последовательности: " +
#                        str(time.time() - base_time) + "\n")
#             file.close()
#             return -1
#
#         cur = pow(a, i, p)
#         if cur in vals:
#             file.write("t = " + str(i) + "\n")
#             file.write("время затраченное на поиск значения t: " +
#                        str(time.time() - base_time) + "\n")
#             file.close()
#             return (vals.index(cur) * s + i) % q
#     file.close()
#

def Gelfond(a, b, p, q):
    file = open("gelfond.txt", "a")
    s = int(math.sqrt(q) + 0.5) + 1
    print(s)
    vals = {}
    s_1 = reverseMod(pow(a, s, p), p)
    start = time.time()
    for i in range(0, s):
        if time.time() - start > 7200:
            for key, value in list(vals.items())[:5]:
                file.write("{ " + str(key) + ", " + str(value) + " }\n")
            file.write("\n")
            for key, value in list(vals.items())[-5:]:
                file.write("{ " + str(key) + ", " + str(value) + " }\n")
            file.write("\n")
            # for j in range(5):
            # file.write(
            # "{ " + str(array[j][0]) + ", " + str(array[j][1]) + " }\n")
            file.close()
            return -1
        cur = (b * pow(s_1, i, p)) % p
        # if i < 6:
        # file.write(str(i) + ", " + str(cur) + "\n")
        # else:
        # for j in range(4):
        #     array[j] = array[j + 1][:]
        # array[4][0] = i
        # array[4][1] = cur

        if cur not in vals:
            vals[cur] = i

    vals = OrderedDict(sorted(vals.items()))

    base_time = time.time()

    for key, value in list(vals.items())[:5]:
        file.write("{ " + str(key) + ", " + str(value) + " }\n")
    file.write("\n")
    for key, value in list(vals.items())[-5:]:
        file.write("{ " + str(key) + ", " + str(value) + " }\n")
    file.write("\n")

    for i in range(0, s):
        if time.time() - start > 7200:
            file.write(
                "t, до которого построена последовательность a^t (mod p): " + str(i) + "\n")
            file.write("время затраченное на построение второй последовательности: " +
                       str(time.time() - base_time) + "\n")
            file.close()
            return -1
        cur = pow(a, i, p)
        if cur in vals:
            file.write("t = " + str(i) + "\n")
            file.write("время затраченное на поиск значения t: " +
                       str(time.time() - base_time) + "\n")
            file.close()
            return (vals[cur] * s + i) % q
    return -1


# x = Gelfond(3, 5, 799454910203247398939, 399727455101623699469)
x = Gelfond(2, 7, 167, 83)
print(x)
